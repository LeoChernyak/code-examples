package Recursion;

import java.util.ArrayList;
import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Task1 task1 = new Task1();
        System.out.println(task1.factorial(6));
        Task2 task2 = new Task2();
        System.out.println(task2.recursion(1,20));
        System.out.println(task2.recursion(20,1));
        Task3 task3 = new Task3();
        System.out.println(task3.recursion(2,2));
        Task4 task4 = new Task4();
        System.out.println(task4.recursion(202020));
        Task5 task5 = new Task5();
        System.out.println("Task5");
        task5.recursion(567);
        System.out.println("Task6");
        Task6 task6 = new Task6();
        System.out.println(task6.recursion(567));
        System.out.println("Task7");
        Task7 task7 = new Task7();
        task7.recursion(34);
        System.out.println("Task8");
        Task8 task8 = new Task8();
        task8.recursion("level");
        System.out.println("Task9");
        Task9 task9 = new Task9();
        task9.recursion("41345235");
        System.out.println("Task10");
        Task10 task10 = new Task10();
        task10.recursion("41345235");


    }
}
class Task1{
    public int factorial(int n){
        if (n==1){
            return n = 1;
        }
        return n * factorial(n-1);
    }
}
class Task2{
    public  int recursion(int a, int b){
        if (a<b){
            System.out.print(a + " ");
            return recursion(a+1,b);
        }
        else if (a>b){
            System.out.print(a + " ");
            return recursion(a-1,b);
        }
        return a;
    }
}


class Task3{
    public int recursion(int m, int n){
        if (m==0){
            System.out.println(m + " " + n);
            return (n+1);
        }
        else if (m > 0 && n==0) {
            System.out.println(m + " " + n);
            return recursion(m-1,1);
        }
        else if (m > 0 && n > 0){
            System.out.println(m + " " + n);
            return recursion(m-1,recursion(m,n-1));
        }
    return n;
    }
}


class Task4{
    int sum;

    public int recursion(int n){
        if (n>0) {
            sum = sum + (n % 10);
            n = n / 10;
            return recursion(n);
        }
        return sum;
    }
}


class Task5{

    int print;

    public int recursion(int n){
        if (n>0){
            print = n%10;
            System.out.print(print + " ");
            n = n / 10;
            return recursion(n);
        }
        return print;
    }
}

class Task6 {
    public String recursion(int n) {
        if (n < 10) {
            return Integer.toString(n);
        } else {
            return recursion(n / 10) + " " + n % 10;
        }

    }
}


class  Task7 {
    int counter = 2;
    public int recursion(int n) {
        if (n>1){
            if (n%counter == 0){
                n = n/counter;
                System.out.println(counter + " ");
            }
            else {
                counter++;
            }
            return recursion(n);
        }
        return counter;
    }

}

class Task8 {
    public String recursion(String n){
        String reverse = new StringBuffer(n).reverse().toString();
        if (reverse.equals(n)) {
            System.out.println("yes, its polignom - " + reverse);
        }
        else {
            System.out.println("No - it`s not polignom");
        }
        return reverse;
    }

}

class Task9 {
    //        for (int i = 0; i<n.length();i++) {
//            char c = n.charAt(i);
//            int b = Character.getNumericValue(c);

    int index;
    public void recursion(String n) {
            if (index < n.length()) {
                char c = n.charAt(index);
                int b = Character.getNumericValue(c);
                if (b % 2 != 0) {
                    System.out.print(b + " ");
                }
                index++;
                recursion(n);
            }
        }

    }


    class Task10{
            int index;
            int max;
            public int recursion(String n) {
                if (index < n.length()) {
                    char c = n.charAt(index);
                    int b = Character.getNumericValue(c);
                    if (b>max) {
                        max = b;
                    }
                    System.out.println(max);
                    index++;
                    recursion(n);
                }
                System.out.println(max);
                return max;
            }


}