package MathAlgoritms;

    public class Task3 {
        static int size = 9;
        static int[][] array = new int[size][size];
        static int num = size*size;

        public static void main(String args[]) {
            go("LEFT",size-1,size-1);
            for(int i=0;i<size;i++){
                for(int j=0;j<size;j++){
                    System.out.print(array[i][j]);
                    System.out.print("\t");
                }
                System.out.println();
            }

        }

        static void go(String w,int x, int y){
            if(x==size/2 && y==size/2) {array[y][x]=1;return;}
            if (w == "LEFT") {
                while(array[y][x]==0 && x>=0){
                    array[y][x]=num;
                    x--;
                    num--;
                    if(x==-1) {break;}
                }
                go("UP",x+1,y-1);
            } else if (w == "UP") {
                while(array[y][x]==0 && y>=0){
                    array[y][x]=num;
                    y--;
                    num--;
                    if(y==-1){break;};
                }
                go("RIGHT",x+1,y+1);
            } else if (w == "RIGHT") {
                while(array[y][x]==0 && x<size){
                    array[y][x]=num;
                    x++;
                    num--;
                    if(x==size){break;};
                }
                go("DOWN",x-1,y+1);
            } else if (w == "DOWN") {
                while(array[y][x]==0 && y<size){
                    array[y][x]=num;
                    y++;
                    num--;
                }
                go("LEFT",x-1,y-1);
            }
        }
    }

