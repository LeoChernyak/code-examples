package lesson8;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Main {
    public static <listOfStudents> void main(String[] args) {
        Human Jack = new Human();
        Jack.ident();
        System.out.println();
        Human Lev = new Human("Lev","",22,true);
        Lev.ident();
        System.out.println();


        Monster monster1 = new Monster("SpiderPussy", 150,200);
        monster1.ident();

        Task4 jam = new Task4(4,6);
        jam.ident();
        jam.compare();
        jam.sum();

        System.out.println();
        System.out.println("Задача 4");
        System.out.println();


        Student[] listOfStudents = new Student[10];
        for (int i = 0; i< listOfStudents.length;i++){
            int[] progress = new int[5];
            for (int j = 0; j < progress.length;j++){
                progress[j] = (int) (Math.random()*5) + 1;
            }
            listOfStudents[i] = new Student("Dave Johns", "Cherniak", i+2, progress);

            for (int z = 0; z < progress.length; z++)
            if(progress[z] == 5 || progress[z] == 4){
                System.out.println(listOfStudents[i].print());
            }
        }
    }
}

 class Human{

    String name;
    String lastName;
    int age;
    boolean sex = false;

    void ident(){
        System.out.println("name = " + name);
        System.out.println("lastname = " + lastName);
        System.out.println("Age = " + age);
        System.out.println("sex = " + sex);

    }
     Human(){
         name = "Jack";
         lastName = "Johnson";
         age = 20;
         sex = true;
     }

     Human(String name, String lastName, int age, boolean sex){
         this.name = name;
         this.lastName = lastName;
         this.age = age;
         this.sex = sex;
     }





}
class Monster{
    String name;
    int hp;
    double damage;
    double armor;

    void ident(){
        System.out.println("Name = " + name);
        System.out.println("Health = " + hp);
        System.out.println("Damage = " + damage);
        System.out.println("Armor = " + armor);
    }
    Monster(String name, double damage, double armor){
        this.name = name;
        hp = 100;
        this.damage = damage;
        this.armor = armor;
    }
}

class Task4{
    int num1;
    int num2;

    void ident(){
        System.out.println( "Числа " + num1 + " и  " + num2);

    }
    Task4(int num1, int num2){
        this.num1 = num1;
        this.num2 = num2;
    }
    void sum() {
        int sum = num1 + num2;
        System.out.println("Сумма равна " + sum);
    }

    void compare(){

        if (num2 > num1) {
            System.out.println("Максимальное значение " + num2);
        }
        else {
            System.out.println("Максимальное значение " + num1);
        }
    }
}

class Student {
    String nameshort;
    String lastname;
    int numOfGroup;
    int[] progress;
    int average;


    Student(String nameshort, String lastname, int numOfGroup, int[] marks) {
        this.nameshort = nameshort;
        this.lastname = lastname;
        this.numOfGroup = numOfGroup;
        this.progress = marks;

    }
    String print() {
        return "ФИО: " + nameshort + " " + lastname + ", номер группы: " + numOfGroup + ", успеваемость: " + Arrays.toString(progress);
    }


}




