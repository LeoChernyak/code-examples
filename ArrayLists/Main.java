package ArrayLists;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

public class Main {
    public static void main(String[] args) throws IOException {
        ArrayList<String> list = new ArrayList<>();
        Main main = new Main();
//        main.task1(list);
//        main.task2(list);
//        main.task3(list);
//        main.task4(list);
//        main.task5(list);
//        main.task6(list);

    }


    void task1(ArrayList<String> list){
        System.out.println("Task1");
        for (int i = 0; i < 10; i++){
            list.add("String");
            System.out.println(list.get(i));
        }
    }

    void task2(ArrayList<String> list) throws IOException {
        System.out.println("Task2");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int i = 0; i < 10; i++){
            list.add(reader.readLine());
        }
        String max = list.get(0);
        for (String i : list){
            if (max.length() < i.length()){
                max = i;

            }
        }
        System.out.println(max);
    }

    void task3(ArrayList<String> list) throws IOException {
        System.out.println("Task3");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int i = 0; i < 10; i++){
            list.add(reader.readLine());
        }
        String min = list.get(0);
        for (String i : list){
            if (min.length() > i.length()){
                min = i;
            }
        }
        System.out.println(min);
    }

    void task4(ArrayList<String> list) throws IOException {
        System.out.println("Task4");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int i = 0; i < 10; i++){
            list.add(0,reader.readLine());
        }
        for (String i : list){
            System.out.println(i);
        }
    }

    void task5(ArrayList<String> list) throws IOException {
        System.out.println("Task5");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> list1 = new ArrayList<String>();
        for (int i = 0; i < 10; i++){
            list.add(reader.readLine());
        }
        Collections.rotate(list,17);

        for (String i : list){
            System.out.print(i + " ");
        }
    }

    void task6(ArrayList<String> list) throws IOException {
        System.out.println("Task6");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int i = 0; i < 10; i++){
            list.add(reader.readLine());
        }

        String max = list.get(0);
        String min = list.get(0);
        int indexMax = 0;
        int indexMin = 0;

        for (int i = 0;i<list.size();i++){
            if (min.length()>list.get(i).length()){
                min = list.get(i);
                indexMin = i;
            }
            if (max.length()<list.get(i).length()){
                max = list.get(i);
                indexMin = i;
            }
        }

        if (indexMax<indexMin){
            System.out.println(list.get(indexMax));
        }
        else {
            System.out.println(list.get(indexMin));
        }



    }
}


