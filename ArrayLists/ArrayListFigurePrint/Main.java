package ArrayLists.ArrayListFigurePrint;

import java.util.ArrayList;
import java.util.Objects;

public class Main {
    public static void main(String[] args) {
        ArrayList<Object> list = new ArrayList<Object>();
        list.add(new Tractor());
        list.add(new Sex());
        list.add(new Virgina());
        list.add(new TFigure());
        list.add(new HuiFigure());

        Object object = list.get((int) (Math.random()*5));
        if (object instanceof Tractor){
            ((Tractor)object).draw();
            list.add(object);
        }
        if (object instanceof Sex){
            ((Sex)object).draw();
            list.add(object);
        }
        if (object instanceof Virgina){
            ((Virgina)object).draw();
            list.add(object);
        }
        if (object instanceof TFigure){
            ((TFigure)object).draw();
            list.add(object);
        }
        if (object instanceof HuiFigure){
            ((HuiFigure)object).draw();
            list.add(object);
        }




    }
}

abstract class Figure{
    abstract void draw();
}


class Tractor extends Figure{
    @Override
    void draw() {
        System.out.println("$$$$$$");
        System.out.println("$$$$$$");
        System.out.println("$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$     $$$$$$    $$$");
        System.out.println("$$     $$$$$$    $$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$");
    }
}

class Sex extends Figure{
    @Override
    void draw() {

        System.out.println("$$$$$$$$  $$$$$$        $$$$$$   $$$$$$$$$$$$   $$");
        System.out.println("$$$$$$$  $$$$$$$        $$$$$$   $$$$$$$$$$$$   ");
        System.out.println("$$$$$$  $$$$$$$$  $$$$$$$$$$$$$$$   $$$$$$   $$$$");
        System.out.println("$$$$  $$$$$$$$$$  $$$$$$$$$$$$$$$   $$$$$$   $$$");
        System.out.println("$$$$$$  $$$$$$$$  $$$$$$$$$$$$$$$$$$      $$$$$$");
        System.out.println("$$$$$$$$  $$$$$$        $$$$$$$$$$$$      $$$$$$");
        System.out.println("$$$$$$$$$$  $$$$        $$$$$$$$$   $$$$$$   $$$");
        System.out.println("$$$$$$$$$$  $$$$  $$$$$$$$$$$$$$$   $$$$$$   $$$");
        System.out.println("$$$$$$$$$  $$$$$  $$$$$$$$$$$$   $$$$$$$$$$$$   $$$$$$$$");
        System.out.println("$$$$$$$$  $$$$$$  $$$$$$$$$$$$   $$$$$$$$$$$$   $$$$$$$$");
        System.out.println("$$$$$$  $$$$$$$$        $$$$$   $$$$$$$$$$$$$$$$$   $$$");
        System.out.println("$$$$  $$$$$$$$$$        $$$$$   $$$$$$$$$$$$$$$$$   $$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");



    }
}

class Virgina extends Figure{
    @Override
    void draw() {

        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$       $$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$       $$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$       $$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$                  $$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$                  $$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$                  $$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$                  $$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$               $$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$               $$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$               $$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$               $$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
    }
}

class TFigure extends Figure{
    @Override
    void draw() {

        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$                                   $$$$$$$$$$$");
        System.out.println("$$$$$$$$$$                                   $$$$$$$$$$$");
        System.out.println("$$$$$$$$$$                                   $$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$                   $$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$                   $$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$                   $$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$                   $$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$               $$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$               $$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$               $$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$               $$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");

    }
}

class HuiFigure extends Figure{
    @Override
    void draw() {
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$        $$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$           $$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$           $$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$           $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$           $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$           $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$           $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$           $$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$           $$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$                                      $$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$             $$$$$$$$$$$              $$$$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$             $$$$$$$$$$$              $$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$             $$$$$$$$$$$              $$$$$$$$$$$$$");
        System.out.println("$$$$$$$$$$$             $$$$$$$$$$$              $$$$$$$$$$$$$");
    }
}

