package eNum;

import java.util.Scanner;

public class Meeting {
    String nameOfMeeting;
    DayofWeek dayofWeek;
    Months months;
    int numOfDat;
    String description;
    int hours;
    int minutes;

    public Meeting(DayofWeek dayofWeek, Months months) {

        this.dayofWeek = dayofWeek;
        this.months = months;

    }

    void show() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите название события");
        nameOfMeeting = scanner.nextLine();
        System.out.println("Введите описание");
        description = scanner.nextLine();
        System.out.println("Введите день месяца");
        numOfDat = scanner.nextInt();
        System.out.println("Введите время");
        hours = scanner.nextInt();
        System.out.println("Введите минуты");
        minutes = scanner.nextInt();

        System.out.println("Имя встречи: " + nameOfMeeting + "; Месяц: "+ months + "; День недели: " + dayofWeek + "; День месяца: "+ numOfDat + "; Описание: " + description + "; Часы: " + hours + "; Минуты: " + minutes);
    }
}
