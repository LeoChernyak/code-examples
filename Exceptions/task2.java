package Exceptions;

import java.util.Scanner;

public class task2 {
    public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);
    String n = scanner.nextLine();
    int num = Integer.parseInt(n);
    try {
        if ((num < (-71)) || (num > 14)) throw new GetException("Ошибка",num);
        double function2 = 12*num + 44;
        System.out.println(function2);

    }
        catch (GetException ex) {
//            System.out.println("Ошибка");
            System.out.println(          ex.getMessage() + " " + num);
        }
    }
}

class GetException extends Exception{
    //String message;
    int num;

    public GetException(String message, int num) {
      super(message);
        //  this.message = message;
        this.num = num;
    }


}
