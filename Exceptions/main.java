package Exceptions;

import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String n = scanner.nextLine();
        int num = Integer.parseInt(n);

        System.out.println("Task 1");
        try {
            double function1 = 1 / num;

            System.out.println(function1);
        }
        catch (Exception ex) {
                System.out.println("Вы ввели ноль, на ноль делить нельзя");
        }



        System.out.println("Task2");

        try {
            if ((num < (-71)) || (num > 14)) throw new  Exception();
                double function2 = 12*num + 44;
            System.out.println(function2);

        }
        catch (Exception ex){
            System.out.println("Выход за пределы");
            ex.getMessage();
        }





        System.out.println("Task3");
        System.out.println("Введите число : ");
        String number = scanner.nextLine();
        System.out.println("Введите нижнюю границу промежутка : ");
        String minimal = scanner.nextLine();
        System.out.println("Введите верхнюю границу промежутка : ");
        String maximal = scanner.nextLine();

        int numeral = Integer.parseInt(number);

        int min = Integer.parseInt(minimal);

        int max = Integer.parseInt(maximal);

        try {
            if ((numeral < min) || (numeral > max)) throw new  Exception();
            int function3 = (18*numeral)^2 + (54/numeral) - 8;
            System.out.println(function3);

        }
        catch (Exception ex){
            System.out.println("Выход за пределы");
            ex.getMessage();
        }




    }
}


