package AbstractInterface;

public class ThirdTask {
    public static void main(String[] args) {
        OrcPeon orcPeon = new OrcPeon(12,32,100, "crushing");
        orcPeon.Attack();
        OrcSmasher orcSmasher = new OrcSmasher(15,52,100, "smacking");
        orcSmasher.Attack();
        OrcGrunt orcGrunt = new OrcGrunt(20,43,100, "hacking");
        orcGrunt.Attack();

    }
}

abstract class Orc{
    int damage;
    int defense;
    int hp;
    String attack;

    Orc(int damage, int defense, int hp, String attack){
        this.damage = damage;
        this.defense = defense;
        this.hp = hp;
        this.attack = attack;

    }

    abstract void Attack();
}


class OrcPeon extends Orc{

    OrcPeon (int damage, int defense, int hp,String attack){
        super(damage,defense,hp,attack);

    }

    @Override
    void Attack() {
        System.out.println("Type of attack - " + attack + "! Damage - " + damage );
    }
}

class OrcGrunt extends Orc{

    OrcGrunt (int damage, int defense, int hp, String attack) {
        super(damage, defense, hp,attack);

    }

    @Override
    void Attack() {
        System.out.println("Type of attack - " + attack + "! Damage - " + damage );
        }


}

class OrcSmasher extends Orc {
    OrcSmasher (int damage, int defense, int hp, String attack) {
        super(damage, defense, hp, attack);
    }

    @Override
    void Attack() {
            System.out.println("Type of attack - " + attack + "! Damage - " + damage );
    }

}
