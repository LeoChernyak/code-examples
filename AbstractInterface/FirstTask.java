package AbstractInterface;

public class FirstTask {
    public static void main(String[] args) {
        DomesticDog dog1 = new DomesticDog();
        dog1.Action();
        dog1.Voice();
        ServiceDog dog2 = new ServiceDog();
        dog2.Action();
        dog2.Voice();
        HomelessDog dog3 = new HomelessDog();
        dog3.Action();
        dog3.Voice();
    }
}
abstract class Dog {
    String name;
    char sex;
    int weight;

    void Voice(){}
    void Action(){}

}

class DomesticDog extends Dog{
    String address;
    String organization;
    String area;

    @Override
    void Voice() {
        super.Voice();
        System.out.println("Mrrr");
    }

    @Override
    void Action() {
        super.Action();
        System.out.println("Я бегу");
    }
}
class ServiceDog extends Dog{
    String address;
    String organization;
    String area;

    @Override
    void Voice() {
        super.Voice();
        System.out.println("BARK BARK");
    }

    @Override
    void Action() {
        super.Action();
        System.out.println("Я ПОМОГАЮ");
    }
}
class HomelessDog extends Dog{
    String address;
    String organization;
    String area;

    @Override
    void Voice() {
        super.Voice();
        System.out.println("abgalgjknaf");
    }

    @Override
    void Action() {
        super.Action();
        System.out.println("Я хочу жоать");
    }
}
