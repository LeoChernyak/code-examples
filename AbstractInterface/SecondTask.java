package AbstractInterface;

public class SecondTask {
    public static void main(String[] args) {
        Humster humster = new Humster("Bitch",12, "Red", 23);
        humster.Run();
        humster.Jump();
        humster.Eat();

        Chinchilla chinchilla = new Chinchilla("Bitch",11, "Blue", 285);
        chinchilla.Run();
        chinchilla.Jump();
        chinchilla.Eat();

        Rat rat = new Rat("Bitch",3, "Grey", 440);
        rat.Run();
        rat.Jump();
        rat.Eat();




    }
}

abstract class Rodent {
    String sex;
    int weight;
    String color;
    int speed;

    Rodent(String sex, int weight, String color, int speed){
        this.sex = sex;
        this.weight = weight;
        this.color = color;
        this.speed = speed;
    }

    abstract void Run();
    abstract void Jump();
    abstract void Eat();
}

class Humster extends Rodent{

    Humster(String sex, int weight, String color, int speed) {
        super(sex,weight,color,speed);
    }


    @Override
    void Jump() {
        int res = speed*weight;
        System.out.println(res);
    }

    @Override
    void Eat() {
        System.out.println("I like meat");

    }

    void Run(){
        speed = 20;
        String name = "Humster";
        System.out.println(name +" "+ speed);


    }

}
class Chinchilla extends Rodent{

    Chinchilla(String sex, int weight, String color, int speed){
        super(sex,weight,color,speed);
    }

    @Override
    void Run(){
        int speed = 12;
        String name = "Chinchila";
        System.out.println(name +" "+ speed);


    }

    @Override
    void Jump() {
        int res = speed*weight;
        System.out.println(res);
    }

    @Override
    void Eat() {
        System.out.println("I like apples");
    }

}
class Rat extends Rodent {

    Rat(String sex, int weight, String color, int speed){
        super(sex,weight,color,speed);
    }

    @Override
    void Run(){
        int speed = 30;
        String name = "Humster";
        System.out.println(name +" "+ speed);


    }

    @Override
    void Jump() {
        int res = speed*weight;
        System.out.println(res);
    }

    @Override
    void Eat() {
        System.out.println("I like cereal");
    }

}
