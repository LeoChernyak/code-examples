package AbstractInterface;

import java.util.Scanner;

public class TikTakToe {
    public static void main(String[] args) {
        Human human = new Human();
        Computer computer = new Computer();
        Game game = new Game(human,computer);
        game.play();
    }
}


interface Player{

    String getName();
    int getMove();
    int addPoint();
    int getPoints();
}

class Human implements Player{
    int counterHuman;
    int tempHuman;


    @Override
    public String getName() {
            System.out.println("Введите ваше имя!");
            Scanner scanner = new Scanner(System.in);
            String name = scanner.nextLine();
            return name;
    }

    @Override
    public int getMove() {
        Scanner scanner = new Scanner(System.in);
        int move = Integer.parseInt(scanner.nextLine());
        tempHuman = move;
try {

    if (move == 1){
        System.out.println("Камень");
    } else if (move == 2) {
        System.out.println("Ножницы");
    } else if (move == 3) {
        System.out.println("Бумага");
    } else if (move > 3 || move <=0) throw new Exception();
}
catch (Exception ex) {
        System.out.println("Рукожоп тормазнутый - смотри на кнопки");
        getMove();
    ex.getMessage();

}
        return tempHuman;
    }

    @Override
    public int addPoint() {
        counterHuman++;
        return counterHuman;
    }

    @Override
    public int getPoints() {
        System.out.println(counterHuman);
        return counterHuman;
    }
}



class Computer implements Player {

    int counterComp;
    int tempComp;

    @Override
    public String getName() {
        String name = "Computer";
        System.out.println("Вы играете против " + name);
        return name;
    }

    @Override
    public int getMove() {
        int a = 1;
        int b = 3;
        int move = a + (int) (Math.random() * b);
        tempComp = move;

        if (move == 1) {
            System.out.println("Камень");
        } else if (move == 2) {
            System.out.println("Ножницы");
        } else if (move == 3) {
            System.out.println("Бумага");
        }
        return tempComp;
    }

    @Override
    public int addPoint() {
        counterComp++;
        return counterComp;
    }

    @Override
    public int getPoints() {
        System.out.println(counterComp);
        return counterComp;
    }
}

class Game {

    int i = 0;
    Human human;
    Computer computer;


    Game(Human human, Computer computer) {
        this.human = human;
        this.computer = computer;
    }

    void play() {
        int i = 0;
        human.getName();
        computer.getName();
       while (i < 10) {
           human.getMove();
           computer.getMove();
           if (human.tempHuman > computer.tempComp) {
               human.addPoint();
               System.out.println("Вы выиграли");
           } else {
               computer.addPoint();
               System.out.println("Вы проиграли");
           }
           i++;
       }
       human.getPoints();
       computer.getPoints();
       if (human.counterHuman > computer.counterComp){
           System.out.println("Вы победили со счетом n" + human.counterHuman + ":" + computer.counterComp);
       }
       else {
           System.out.println("Вы проиграли со счетом " + human.counterHuman + ":" + computer.counterComp);
       }


    }
}




