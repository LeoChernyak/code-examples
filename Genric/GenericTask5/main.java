package Genric.GenericTask5;

public class main {
    public static void main(String[] args) {
        ElfArcher elfArcher = new ElfArcher(100,40);
        System.out.println(elfArcher.toString());
        ElfMage elfMage = new ElfMage(100,20);
        System.out.println(elfArcher.toString());
        ElfWarrior elfWarrior = new ElfWarrior(100,50);
        System.out.println(elfWarrior.toString());

    }
}

class Elf <T> {
    T health;
    T damage;

    public Elf(T health, T damage) {
        this.health = health;
        this.damage = damage;
    }

    public void Strike(){

    }
}

class ElfWarrior extends Elf{

    public ElfWarrior(Object health, Object damage) {
        super(health, damage);
    }

    @Override
    public String toString() {
        return "ElfWarrior{" +
                "health=" + health +
                ", damage=" + damage +
                '}';
    }

    @Override
    public void Strike() {
        super.Strike();



    }
}

class ElfArcher extends Elf{

    public ElfArcher(Object health, Object damage) {
        super(health, damage);
    }

    @Override
    public void Strike() {
        super.Strike();
    }

    @Override
    public String toString() {
        return "ElfArcher{" +
                "health=" + health +
                ", damage=" + damage +
                '}';
    }
}

class ElfMage extends Elf{

    public ElfMage(Object health, Object damage) {
        super(health, damage);
    }

    @Override
    public void Strike() {
        super.Strike();
    }

    @Override
    public String toString() {
        return "ElfMage{" +
                "health=" + health +
                ", damage=" + damage +
                '}';
    }
}


class Generalization <T> extends Elf <T> {

    public Generalization(T health, T damage) {
        super(health, damage);
    }



}