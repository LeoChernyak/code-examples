package inhertiance2;

import java.lang.ref.PhantomReference;

public class Product {


    private String name;
    private int price;
    private int weight;


    public Product(String name, int price,int weight){
        this.weight=weight;
        this.price = price;
        this.name = name;
    }
    Product() {}


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight =(weight);
    }


}
