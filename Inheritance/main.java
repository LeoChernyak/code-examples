package Inheritance;

public class main {
    public static void main(String[] args) {
        Calculator calc1 = new Calculator();
        GetSet getSet = new GetSet();
        System.out.println(calc1.plus());
        System.out.println(calc1.minus());
        System.out.println(calc1.multi());
        System.out.println(calc1.delight());
        System.out.println(calc1.square());
        System.out.println(calc1.trigcos());
        System.out.println();
        System.out.println(getSet.bigSum());

    }
}
