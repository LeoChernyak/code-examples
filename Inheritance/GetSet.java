package Inheritance;

public class GetSet extends Calculator {


    public double getNum() {
        return num;
    }

    public void setNum(double num) {
        this.num = num;
    }

    public double bigSum() {
        this.num++;
        return this.num;
    }


}
