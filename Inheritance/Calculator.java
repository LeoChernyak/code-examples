package Inheritance;

public class Calculator {



    double num = 10;


    public double plus() {
        double sum = 0;
        sum = num + 10;

        return sum;
    }

    public double minus() {
        double minus = 0;
        minus = num - 30;

        return minus;
    }

    public double multi() {
        double multi = 0;
        multi = num * 10;
        return multi;
    }

    public double delight() {
        double delight = 0;
        delight = num/2;
        return delight;

    }

    public double square() {
        double square = 0;
        square = Math.sqrt(num);
        return square;
    }

    public double trigcos() {
        double cos = 0;
        cos = Math.cos(num);
        return cos;
    }

}
